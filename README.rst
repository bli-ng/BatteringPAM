=============
Battering PAM
=============

A framework for developing PAM Backdoors in Python. Requires pam_python.so on the system.

leaker.py
---------

Designed to be placed at the end of an authentication stack an import a ``leak`` function to do your dirty work. The leak function is passed ``creds``, a dict containing ``service``, ``rhost``, ``user``, and ``authtok``
